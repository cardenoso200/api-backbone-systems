# Reto Técnico
## [Backbone System](https://jobs.backbonesystems.io/challenge/1)

Instrucciones del reto.
El reto consiste en utilizar el framework Laravel para replicar la funcionalidad de esta api
##### (https://jobs.backbonesystems.io/api/zip-codes/01210), utilizando esta fuente de información.
El tiempo de respuesta promedio debe ser menor a 300 ms, pero entre menor sea, mejor.
##### [GET] https://jobs.backbonesystems.io/api/zip-codes/{zip_code}

#### Pasos de  la solución del reto!
1. Se descargó la información de los códigos postales del siguiente  enlace [https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/CodigoPostal_Exportar.aspx](https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/CodigoPostal_Exportar.aspx)
en la cual se validó los tipos de datos y  la verificación como llegaba la data.

3. Se hizo una migración para crear la tabla zip_codes llamado CreateZipCodesTable.php 

## CreateZipCodesTable.php 
```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZipCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zip_codes', function (Blueprint $table) {
            $table->string("d_codigo",10);
            $table->string("d_asenta",120);
            $table->string("d_tipo_asenta",120);
            $table->string("D_mnpio",120);
            $table->string("d_estado",120);
            $table->string("d_ciudad",120)->nullable();
            $table->string("d_CP",120)->nullable();
            $table->string("c_estado",120)->nullable();
            $table->string("c_oficina",120);
            $table->string("c_CP",10)->nullable();
            $table->string("c_tipo_asenta",10)->nullable();
            $table->string("c_mnpio",10)->nullable();
            $table->string("id_asenta_cpcons",10)->nullable();
            $table->string("d_zona",120);
            $table->string("c_cve_ciudad",120)->nullable();
            $table->index('d_codigo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zip_codes');
    }
}

``` 
2. Luego de obtener la data clasificada, se creó un seeder llamado ZipCodeSeeder.

3. Y por ultimó se creó un controlador llamado ZipCodeContoller.php y un modelo ZipCodes.php , en la cual se hizo la lógica para replicar como el api del endpoint https://jobs.backbonesystems.io/api/zip-codes/{zip_code} entregaba la información.

## ZipCodes.php
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZipCodes extends Model
{
    use HasFactory;
    public $timestamps = false;
    public $tables     = 'zip_codes';
}
?>

``` 
## ZipCodeContoller.php 

```php
<?php

namespace App\Http\Controllers\API;

use App\Models\ZipCodes;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;

class ZipCodeController extends Controller
{
    /**
     * Returns city information according to zip code in JSON format.
     *
     * @param string $zipCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFetchZipCode(Request $request)
    {
            $zipCode     = $request->zip_code;
            $zipCodeInfo = $this->fetchZipCode($zipCode);
            return response()->json($zipCodeInfo);
       
    }

    /**
     * Returns an array of city information based on zip code.
     *
     * @param string $zipCode
     * @return array
     */
    public function fetchZipCode($code)
    {
        try
        {
            $zipCodesList = ZipCodes::where('d_codigo',$code)->get();
            $zipCode      = $zipCodesList->first();
            
            if(empty($zipCode)){
                return response()->json([
                        'message'=>'Zip Código no existe'
                    ],422);
            }

            $dEstado  = $this->removeAccents($zipCode->d_estado);
            $dMnpio   = $this->removeAccents($zipCode->D_mnpio);
            $dCiudad  = $this->removeAccents($zipCode->d_ciudad);

            $federalEntity = [
                'key'  => (int)$zipCode->c_estado,
                'name' => $this->strUpperCase($dEstado),
                'code' => $zipCode->c_cp ?: null,
            ];

            $municipality = [
                'key'  => (int)$zipCode->c_mnpio,
                'name' => $this->strUpperCase($dMnpio),
            ];
            
            $locality = $this->strUpperCase($dCiudad);

            $settlements = $zipCodesList->map(function ($zipCode){
                $dAsenta = $this->removeAccents($zipCode->d_asenta);
                $dZona   = $this->removeAccents($zipCode->d_zona);
            
                return [
                    'key'             => (int)$zipCode->id_asenta_cpcons,
                    'name'            => $this->strUpperCase($dAsenta),
                    'zone_type'       => $this->strUpperCase($dZona),
                    'settlement_type' => [
                        'name'        => $zipCode->d_tipo_asenta,
                    ],
                ];
            });
    
            return [
                'zip_code'        => $code,
                'locality'        => $locality,
                'federal_entity'  => $federalEntity,
                'settlements'     => $settlements,
                'municipality'    => $municipality,
            ];

        }catch(Exception $e){
            Log::info([
                    'error'    =>'error en el api zipcodecontroller@fetchZipCode',
                    'messagge' => $e->getMessage(),
                    'line'     => $e->getLine(),
                ]
            );
        }
    }
    /**
     * Converts a string to uppercase and title case.
     *
     * @param string $string
     * @return string
     */
    private function strUpperCase($string)
    {
        return ucwords(mb_strtoupper($string));
    }
    public function removeAccents($string){
        $search  = array('á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú','ñ');
        $replace = array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U','n');
        $textWithAccents = str_replace($search, $replace, $string);
        return $textWithAccents;
    }
   
}

``` 
#### Instalación del proyecto en Amazon aws 

Se creo una instancia en ubuntu  y se replico el docker. Esta es la url del endpoint
##### [http://44.202.233.123/api/zip-codes/01210](http://44.202.233.123/api/zip-codes/01210)


![Texto alternativo](instancia.png)

#### Especificación del proyecto

- PHP 8.0.28
- Laravel Framework 9
- Mysql 5.7 
- Docker Versión 23.0.1

#### Instalación del proyecto

1. Clonar el repositorio https://gitlab.com/cardenoso200/api-backbone-systems.git

2. Instalar Docker y Docker-Compose.

3. Correr el comando docker-composer up --build

4. Una ve construido los dos contenedores, tanto laravel-api-endpoint-zip-code y mysql-api-endpoint-zip-code,
entrar al contenedor de laravel-api-endpoint-zip-code, con este comando.
 ```bash
   docker exec -it laravel-api-endpoint-zip-code bash.
 ```

5. Instalar el composer del proyecto.  
  ```bash
    composer install 
 ```
6. Generar el key de la aplicación.  
  ```bash
    php artisan key:generate
 ```

7. Crear las bases de datos.
  ```bash
    php artisan migrate
 ```

8. Correr el seeder para  insertar la data, en la bases de datos.
  ```bash
    php artisan db:seed
 ```
