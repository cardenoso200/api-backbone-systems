<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZipCodes extends Model
{
    use HasFactory;
    public $timestamps = false;
    public $tables     = 'zip_codes';
}
