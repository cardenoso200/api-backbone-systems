<?php

namespace App\Http\Controllers\API;

use App\Models\ZipCodes;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;

class ZipCodeController extends Controller
{
    /**
     * Returns city information according to zip code in JSON format.
     *
     * @param string $zipCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFetchZipCode(Request $request)
    {
            $zipCode     = $request->zip_code;
            $zipCodeInfo = $this->fetchZipCode($zipCode);
            return response()->json($zipCodeInfo);
       
    }

    /**
     * Returns an array of city information based on zip code.
     *
     * @param string $zipCode
     * @return array
     */
    public function fetchZipCode($code)
    {
        try
        {
            $zipCodesList = ZipCodes::where('d_codigo',$code)->get();
            $zipCode      = $zipCodesList->first();
            
            if(empty($zipCode)){
                return response()->json([
                        'message'=>'Zip Código no existe'
                    ],422);
            }

            $dEstado  = $this->removeAccents($zipCode->d_estado);
            $dMnpio   = $this->removeAccents($zipCode->D_mnpio);
            $dCiudad  = $this->removeAccents($zipCode->d_ciudad);

            $federalEntity = [
                'key'  => (int)$zipCode->c_estado,
                'name' => $this->strUpperCase($dEstado),
                'code' => $zipCode->c_cp ?: null,
            ];

            $municipality = [
                'key'  => (int)$zipCode->c_mnpio,
                'name' => $this->strUpperCase($dMnpio),
            ];
            
            $locality = $this->strUpperCase($dCiudad);

            $settlements = $zipCodesList->map(function ($zipCode){
                $dAsenta = $this->removeAccents($zipCode->d_asenta);
                $dZona   = $this->removeAccents($zipCode->d_zona);
            
                return [
                    'key'             => (int)$zipCode->id_asenta_cpcons,
                    'name'            => $this->strUpperCase($dAsenta),
                    'zone_type'       => $this->strUpperCase($dZona),
                    'settlement_type' => [
                        'name'        => $zipCode->d_tipo_asenta,
                    ],
                ];
            });
    
            return [
                'zip_code'        => $code,
                'locality'        => $locality,
                'federal_entity'  => $federalEntity,
                'settlements'     => $settlements,
                'municipality'    => $municipality,
            ];

        }catch(Exception $e){
            Log::info([
                    'error'    =>'error en el api zipcodecontroller@fetchZipCode',
                    'messagge' => $e->getMessage(),
                    'line'     => $e->getLine(),
                ]
            );
        }
    }
    /**
     * Converts a string to uppercase and title case.
     *
     * @param string $string
     * @return string
     */
    private function strUpperCase($string)
    {
        return ucwords(mb_strtoupper($string));
    }
    public function removeAccents($string){
        $search  = array('á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú','ñ');
        $replace = array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U','n');
        $textWithAccents = str_replace($search, $replace, $string);
        return $textWithAccents;
    }
   
}
